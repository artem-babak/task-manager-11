package ru.t1c.babak.tm.repository;

import ru.t1c.babak.tm.api.repository.ITaskRepository;
import ru.t1c.babak.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public Task findOneById(String id) {
        for (final Task task : tasks) {
            if(id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findOneByIndex(Integer index) {
        try {
            return tasks.get(index);
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    @Override
    public Task add(final Task task) {
        tasks.add(task);
        return task;
    }

    @Override
    public Task create(final String name) {
        final Task task = new Task();
        task.setName(name);
        return add(task);
    }

    @Override
    public Task create(final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return add(task);
    }

    @Override
    public Task remove(final Task task) {
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeById(final String id) {
        final Task task = findOneById(id);
        if (task == null) return null;
        return remove(task);
    }

    @Override
    public Task removeByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        return remove(task);
    }

    @Override
    public void clear() {
        tasks.clear();
    }

}
