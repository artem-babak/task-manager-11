package ru.t1c.babak.tm.api.controller;

public interface ICommandController {
    void showWelcome();

    void showSystemInfo();

    void showAbout();

    void showVersion();

    void showCommands();

    void showArguments();

    void showHelp();

    void showErrorArgument(String arg);

    void showErrorCommand(String arg);
}
