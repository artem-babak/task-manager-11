package ru.t1c.babak.tm.api.service;

import ru.t1c.babak.tm.model.Task;

import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task updateOneById(String id, String name, String description);

    Task updateOneByIndex(Integer index, String name, String description);

    Task add(Task task);

    Task create(String name);

    Task create(String name, String description);

    Task remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    void clear();

}
