package ru.t1c.babak.tm.api.repository;

import ru.t1c.babak.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task add(Task task);

    Task create(String name);

    Task create(String name, String description);

    Task remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

//    Task removeByName(String name);

    void clear();

}
