package ru.t1c.babak.tm.api.controller;

public interface IProjectController {

    void showProjectList();

    void createProject();

    void clearProjects();

    void showProjectById();

    void showProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

    void removeProjectById();

    void removeProjectByIndex();

}
